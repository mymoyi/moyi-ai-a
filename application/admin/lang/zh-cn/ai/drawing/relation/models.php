<?php

return [
    'Server_id'   => '服务器ID',
    'Models_id'   => '模型ID',
    'Model_hash'  => '模型hash',
    'Models_name' => '名称',
    'Createtime'  => '添加时间'
];
