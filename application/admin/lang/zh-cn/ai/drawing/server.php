<?php

return [
    'Offline'           => '离线',
    'Name'              => '服务器名称',
    'User_id'           => '用户ID',
    'Type'              => '服务器类型',
    'Ip'                => '服务器IP',
    'Port'              => '端口号',
    'Gpu_serial'        => '显卡序列号',
    'Gpu_name'          => '显卡名称',
    'Gpu_ram'           => '显卡内存',
    'Ram'               => '内存',
    'Rank'              => '级别',
    'Status'            => '状态',
    'Status normal'     => '正常',
    'Status processing' => '处理中',
    'Status disconnect' => '无法连接',
    'Status disable'    => '禁用',
    'Status free'       => '空闲',
    'Status wait_init'  => '等待初始化',
    'Status pending'    => '待审核',
    'Createtime'        => '添加时间',
    'Updatetime'        => '更新时间',
    'Task_id'           => '任务ID',
    'Usage'             => '设备状态',
    'Usage processing'  => '处理中',
    'Usage disconnect'  => '无法连接',
    'Usage disable'     => '禁用',
    'Usage free'        => '空闲'
];