<?php

return [
    'Server_id'  => '设备ID',
    'Type'       => '状态',
    'Type info'  => '信息',
    'Type error' => '错误',
    'Type warn'  => '警告',
    'Task_id'    => '任务ID',
    'Content'    => '内容',
    'Createtime' => '添加时间'
];
