<?php

return [
    'Name'              => '名称',
    'Content'           => '介绍',
    'Suggest'           => '配置建议',
    'Status'            => '状态',
    'Status normal'     => '正常',
    'Status hidden'     => '隐藏',
    'Status pending'    => '待审核',
    'Status rejected'   => '驳回',
    'Status sexy'       => '性感',
    'Status classified' => '归类',
    'Status archived'   => '存档',
    'Createtime'        => '添加时间',
    'Updatetime'        => '更新时间'
];
