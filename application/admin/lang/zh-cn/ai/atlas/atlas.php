<?php

return [
    'User_id'           => '用户ID',
    'Total'             => '图集数量',
    'Type'              => '类型',
    'Rank'              => '级别',
    'Views'             => '浏览量',
    'Shares'            => '分享量',
    'Downloads'         => '下载量',
    'Status'            => '状态',
    'Status normal'     => '正常',
    'Status hidden'     => '隐藏',
    'Status pending'    => '待审核',
    'Status rejected'   => '驳回',
    'Status sexy'       => '性感',
    'Status classified' => '归类',
    'Status archived'   => '存档',
    'Createtime'        => '添加时间',
    'Updatetime'        => '更新时间'
];
