<?php

return [
    'Atlas_id'      => '图集ID',
    'Picture_id'    => '图片ID',
    'Status'        => '状态',
    'Status normal' => '正常',
    'Status hidden' => '隐藏',
    'Createtime'    => '添加时间'
];
