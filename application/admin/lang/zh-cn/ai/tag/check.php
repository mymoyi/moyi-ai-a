<?php

return [
    'Picture_id'    => '图片ID',
    'Label'         => '标签',
    'Score'         => '评分',
    'Status'        => '状态',
    'Status normal' => '正常',
    'Status hidden' => '隐藏',
    'Createtime'    => '添加时间',
    'Updatetime'    => '更新时间'
];
