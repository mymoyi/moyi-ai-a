<?php

return [
    'Label'          => '标签',
    'Text'           => '文本',
    'Status'         => '状态',
    'Status normal'  => '正常',
    'Status hidden'  => '隐藏',
    'Status pending' => '待审核',
    'Createtime'     => '添加时间',
    'Updatetime'     => '更新时间'
];
