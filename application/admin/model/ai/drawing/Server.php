<?php

namespace app\admin\model\ai\drawing;

use think\Model;


class Server extends Model
{

    

    

    // 表名
    protected $name = 'ai_drawing_server';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text',
        'usage_text'
    ];
    

    
    public function getStatusList()
    {
        return ['normal' => __('Status normal'), 'processing' => __('Status processing'), 'disconnect' => __('Status disconnect'), 'disable' => __('Status disable'), 'free' => __('Status free'), 'wait_init' => __('Status wait_init'), 'pending' => __('Status pending')];
    }

    public function getUsageList()
    {
        return ['processing' => __('Usage processing'), 'disconnect' => __('Usage disconnect'), 'disable' => __('Usage disable'), 'free' => __('Usage free'), 'offline' => __('Offline')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getUsageTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['usage']) ? $data['usage'] : '');
        $list = $this->getUsageList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}