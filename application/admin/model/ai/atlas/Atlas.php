<?php

namespace app\admin\model\ai\atlas;

use think\Model;


class Atlas extends Model
{

    

    

    // 表名
    protected $name = 'ai_atlas';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text'
    ];
    

    
    public function getStatusList()
    {
        return ['normal' => __('Status normal'), 'hidden' => __('Status hidden'), 'pending' => __('Status pending'), 'rejected' => __('Status rejected'), 'sexy' => __('Status sexy'), 'classified' => __('Status classified'), 'archived' => __('Status archived')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function user()
    {
        return $this->hasOne('User','id','user_id',[],'left')->setEagerlyType(0);
    }
}