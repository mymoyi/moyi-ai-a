<?php

return [
    'autoload' => false,
    'hooks' => [
        'config_init' => [
            'editpage',
            'summernote',
        ],
    ],
    'route' => [],
    'priority' => [],
    'domain' => '',
];
