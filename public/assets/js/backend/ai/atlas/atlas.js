define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ai/atlas/atlas/index' + location.search,
                    add_url: 'ai/atlas/atlas/add',
                    edit_url: 'ai/atlas/atlas/edit',
                    del_url: 'ai/atlas/atlas/del',
                    multi_url: 'ai/atlas/atlas/multi',
                    import_url: 'ai/atlas/atlas/import',
                    table: 'ai_atlas',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'total', title: __('Total')},
                        {field: 'type', title: __('Type'), operate: 'LIKE'},
                        {field: 'rank', title: __('Rank')},
                        {field: 'views', title: __('Views')},
                        {field: 'shares', title: __('Shares')},
                        {field: 'downloads', title: __('Downloads')},
                        {field: 'status', title: __('Status'), searchList: {"normal":__('Status normal'),"hidden":__('Status hidden'),"pending":__('Status pending'),"rejected":__('Status rejected'),"sexy":__('Status sexy'),"classified":__('Status classified'),"archived":__('Status archived')}, formatter: Table.api.formatter.status},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
