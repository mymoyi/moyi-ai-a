define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ai/picture/index' + location.search,
                    add_url: 'ai/picture/add',
                    edit_url: 'ai/picture/edit',
                    del_url: 'ai/picture/del',
                    multi_url: 'ai/picture/multi',
                    import_url: 'ai/picture/import',
                    table: 'ai_picture',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'preview', title: __('Preview'), formatter: Controller.api.formatter.thumb, operate: false},
                        {field: 'url', title: __('Url'), operate: 'LIKE', formatter: Table.api.formatter.url, visible: false},
                        {field: 'title', title: __('Title'),formatter: Controller.api.formatter.title},
                        {field: 'story', title: __('Story'),formatter: Controller.api.formatter.story},
                        
                        {field: 'width', title: __('Width'), visible: false},
                        {field: 'height', title: __('Height'), visible: false},
                        {field: 'model_hash', title: __('Model_hash'), operate: 'LIKE', visible: false},
                        {field: 'model_name', title: __('Model_name'), operate: 'LIKE', visible: false},
                        {field: 'sd_model_hash', title: __('Sd_model_hash'), operate: 'LIKE', visible: false},
                        {field: 'sd_vae', title: __('Sd_vae'), operate: 'LIKE', visible: false},
                        {field: 'sampler_name', title: __('Sampler_name'), operate: 'LIKE', visible: false},
                        {field: 'steps', title: __('Steps'), visible: false},
                        {field: 'cfg_scale', title: __('Cfg_scale'), operate:'BETWEEN', visible: false},
                        {field: 'score', title: __('Score'), operate:'BETWEEN', visible: false},
                        {field: 'seeds', title: __('Seeds'), visible: false},
                        {field: 'subseeds', title: __('Subseeds'), visible: false},
                        {field: 'metadata', title: __('Metadata'), visible: false},
                        {field: 'source', title: __('Source'), operate: 'LIKE', visible: false},
                        {field: 'status', title: __('Status'), searchList: {"normal":__('Status normal'),"hidden":__('Status hidden'),"pending":__('Status pending'),"rejected":__('Status rejected'),"sexy":__('Status sexy'),"classified":__('Status classified'),"archived":__('Status archived')}, formatter: Table.api.formatter.status},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime, visible: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                story: function (value, row, index) {
                    return '<div class="title-element" style="max-width:250px">' + value + '</div>'
                },
                title: function (value, row, index) {
                    return '<div class="title-element">' + value + '</div>'
                },
                thumb: function (value, row, index) {
                   html = '<a href="' + row.url + '" target="_blank"><img src="' + row.url + '?x-oss-process=image/resize,m_lfit,w_120" alt="" style="max-height:60px;max-width:120px"></a>';
                    return '<div style="width:120px;margin:0 auto;text-align:center;overflow:hidden;white-space: nowrap;text-overflow: ellipsis;">' + html + '</div>';
                },
            }
        }
    };
    return Controller;
});