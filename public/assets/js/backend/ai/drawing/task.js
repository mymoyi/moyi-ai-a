define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ai/drawing/task/index' + location.search,
                    add_url: 'ai/drawing/task/add',
                    edit_url: 'ai/drawing/task/edit',
                    del_url: 'ai/drawing/task/del',
                    multi_url: 'ai/drawing/task/multi',
                    import_url: 'ai/drawing/task/import',
                    table: 'ai_drawing_task',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'server_id', title: __('Server_id')},
                        {field: 'title_story', title: __('Title_story')},
                        {field: 'width', title: __('Width')},
                        {field: 'height', title: __('Height')},
                        {field: 'model_hash', title: __('Model_hash'), operate: 'LIKE'},
                        {field: 'vae_hash', title: __('Vae_hash'), operate: 'LIKE'},
                        {field: 'sd_vae', title: __('Sd_vae'), operate: 'LIKE'},
                        {field: 'sampler_name', title: __('Sampler_name'), operate: 'LIKE'},
                        {field: 'steps', title: __('Steps')},
                        {field: 'batch_size', title: __('Batch_size')},
                        {field: 'type', title: __('Type'), operate: 'LIKE'},
                        {field: 'ip', title: __('Ip'), operate: 'LIKE'},
                        {field: 'weight', title: __('Weight')},
                        {field: 'more', title: __('More')},
                        {field: 'status', title: __('Status'), searchList: {"finish":__('Status finish'),"processing":__('Status processing'),"fail":__('Status fail'),"reject":__('Status reject'),"unusual":__('Status unusual'),"wait":__('Status wait')}, formatter: Table.api.formatter.status},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
