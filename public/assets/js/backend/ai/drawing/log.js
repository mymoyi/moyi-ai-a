define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ai/drawing/log/index' + location.search,
                    add_url: 'ai/drawing/log/add',
                    edit_url: 'ai/drawing/log/edit',
                    del_url: 'ai/drawing/log/del',
                    multi_url: 'ai/drawing/log/multi',
                    import_url: 'ai/drawing/log/import',
                    table: 'ai_drawing_log',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'server_id', title: __('Server_id')},
                        {field: 'type', title: __('Type'), searchList: {"info":__('Type info'),"error":__('Type error'),"warn":__('Type warn')}, formatter: Table.api.formatter.normal},
                        {field: 'task_id', title: __('Task_id')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
