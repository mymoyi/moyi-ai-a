define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ai/drawing/server/index' + location.search,
                    add_url: 'ai/drawing/server/add',
                    edit_url: 'ai/drawing/server/edit',
                    del_url: 'ai/drawing/server/del',
                    multi_url: 'ai/drawing/server/multi',
                    import_url: 'ai/drawing/server/import',
                    table: 'ai_drawing_server',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'type', title: __('Type'), operate: 'LIKE'},
                        {field: 'ip', title: __('Ip'), operate: 'LIKE', visible: false},
                        {field: 'port', title: __('Port'), operate: 'LIKE', visible: false},
                        {field: 'gpu_serial', title: __('Gpu_serial'), operate: 'LIKE', visible: false},
                        {field: 'gpu_name', title: __('Gpu_name'), operate: 'LIKE'},
                        {field: 'gpu_ram', title: __('Gpu_ram'), visible: false},
                        {field: 'ram', title: __('Ram'), visible: false},
                        {field: 'rank', title: __('Rank'), visible: false},
                        {field: 'usage', title: __('Usage'), searchList: {"processing":__('Usage processing'),"disconnect":__('Usage disconnect'),"disable":__('Usage disable'),"free":__('Usage free'),"offline":__('Offline')}, formatter: Table.api.formatter.normal},
                        {field: 'status', title: __('Status'), searchList: {"normal":__('Status normal'),"processing":__('Status processing'),"disconnect":__('Status disconnect'),"disable":__('Status disable'),"free":__('Status free'),"wait_init":__('Status wait_init'),"pending":__('Status pending')}, formatter: Table.api.formatter.status},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime, visible: false},
                        {field: 'task_id', title: __('Task_id')},

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});